import re # Importar el módulo de expresiones regulares

def obtener_dominios(fragmento_html):
    # Crear un conjunto para almacenar los dominios únicos
    dominios = set()
    
    # Definir un patrón para buscar enlaces en el fragmento de HTML
    patron_enlace = re.compile(r"https?://(?:www\d*\.)?([\w.-]+)")
    
    # Iterar sobre cada línea del fragmento de HTML
    for linea in fragmento_html:
        # Encontrar todos los enlaces que coincidan con el patrón en la línea actual
        enlaces = patron_enlace.findall(linea)
        
        # Iterar sobre cada enlace encontrado
        for enlace in enlaces:
            # Agregar el dominio a la lista de dominios únicos
            dominios.add(enlace)
    
    # Ordenar los dominios únicos alfabéticamente
    dominios_unicos = sorted(dominios)
    
    # Devolver los dominios únicos como una cadena separada por punto y coma
    return ";".join(dominios_unicos)

# Obtener el número de líneas del fragmento de HTML
n = int(input())

# Leer las líneas del fragmento de HTML y almacenarlas en una lista
fragmento_html = []
for _ in range(n):
    linea = input()
    fragmento_html.append(linea)

# Obtener los dominios únicos del fragmento de HTML
dominios = obtener_dominios(fragmento_html)

# Imprimir los dominios únicos
print('\n', dominios, '\n', sep='')
