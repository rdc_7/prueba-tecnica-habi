import itertools

# Se define de la función 'ordenar_cadena'
def ordenar_cadena(S):
    # Se crea una lista ordenada de letras minúsculas en la cadena 'S'
    letras_min = sorted([c for c in S if c.islower()])
    # Se crea una lista ordenada de letras mayúsculas en la cadena 'S'
    letras_may = sorted([c for c in S if c.isupper()])
    # Se crea una lista ordenada de dígitos impares en la cadena 'S'
    digitos_impares = sorted([c for c in S if c.isdigit() and int(c) % 2 == 1])
    # Se crea una lista ordenada de dígitos pares en la cadena 'S'
    digitos_pares = sorted([c for c in S if c.isdigit() and int(c) % 2 == 0])

    # Se combinan las listas de letras minúsculas, letras mayúsculas, dígitos impares y dígitos pares
    # en una sola y se aplica el método join a una cadena de texto para convertir la lista en una única cadena de texto.
    return ''.join(itertools.chain(letras_min, letras_may, digitos_impares, digitos_pares))

# Se solicita al usuario que ingrese la cadena que desea ordenar
cadena = input("Ingresa la cadena que deseas ordenar: ")

# se llama a la función 'ordenar_cadena' usando la cadena ingresada por el usuario como argumento
# y se almacena el resultado en la variable 'cadena_ordenada'
cadena_ordenada = ordenar_cadena(cadena)

# Se imprime la cadena ordenada
print("Cadena ordenada:", cadena_ordenada)