-- MUESTRA LA CÉDULA Y EL NOMBRE DE LOS BEBEDORES QUE FRECUENTAN SOLO LAS TIENDAS QUE FRECUENTA 'LUIS PEREZ'. --

-- Se selecciona los campos cedula y nombre de la tabla BEBEDOR.
SELECT b.cedula, b.nombre FROM [BEBEDOR] b
-- Se verifica si existe alguna combinación que cumpla ciertas condiciones.
WHERE EXISTS (
	-- Se obtienen las frecuencias de la tabla FRECUENTA.
    SELECT * FROM [FRECUENTA] f
	-- Se debe asegurar que la cédula en la subconsulta coincida con la cedula del bebedor en la consulta principal.
    WHERE f.cedula = b.cedula
	  -- Se verifica si el código de tienda en la subconsulta de frecuencias de Luis Perez está presente en la subconsulta de frecuencias anterior.
      AND f.codigo_tienda IN (
		  -- Se obtiene los codigo_tienda de la tabla FRECUENTA donde el bebedor asociado tiene el nombre 'Luis Perez'
		  -- Se logra haciendo la unión interna entre las tablas FRECUENTA y BEBEDOR.
          SELECT f2.codigo_tienda FROM [FRECUENTA] f2 INNER JOIN [BEBEDOR] b2 ON f2.cedula = b2.cedula
		  -- Por último, se filtran los registros donde el nombre del bebedor es 'Luis Perez' 
          WHERE b2.nombre = 'Luis Perez'
      )
)
-- Si se encuentra al menos un registro en la subconsulta anidada para cada bebedor en la tabla BEBEDOR, 
-- se selecciona ese bebedor y se incluye en los resultados.