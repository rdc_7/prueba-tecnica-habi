-- MUESTRA EL CÓDIGO Y EL NOMBRE DE LAS TIENDAS QUE NO SON FRECUENTADAS POR 'ANDRES CAMILO RESTREPO' --

-- Se selecciona los campos codigo_tienda y nombre_tienda de la tabla TIENDA
SELECT t.codigo_tienda, t.nombre_tienda FROM [TIENDA] t
-- Se combinan la tabla TIENDA con la tabla FRECUENTA en función del campo codigo_tienda.
LEFT JOIN [FRECUENTA] f ON t.codigo_tienda = f.codigo_tienda 
-- Se compara la cédula en la tabla FRECUENTA con la cédula que corresponde a 'Andres Camilo Restrepo' en la tabla BEBEDOR
AND f.cedula = 
(
-- Esto asegura que solo se considere al nombre 'Andres Camilo Restrepo'.
SELECT cedula FROM BEBEDOR WHERE nombre = 'Andres Camilo Restrepo'
)
-- Se aplica una condición para seleccionar solo las tiendas donde la cédula de 'Andres' en la tabla FRECUENTA es nula. 
-- Esto significa que 'Andres' no frecuenta esas tiendas.
WHERE f.codigo_tienda IS NULL;
