-- MUESTRA LA CÉDULA Y EL NOMBRE DE LOS BEBEDORES QUE SOLO FRECUENTAN LAS TIENDAS EN LAS QUE VENDEN LA BEBIDA QUE LES GUSTA. --

-- Se selecciona los campos cedula y nombre de la tabla BEBEDOR.
SELECT b.cedula, b.nombre FROM [BEBEDOR] b
-- Se verifica que no existan registros en la subconsulta anidada.
WHERE NOT EXISTS (
  -- Se busca registros en la tabla FRECUENTA y se hacen dos uniones izquierdas con las tablas VENDE y GUSTA.
  SELECT * FROM [FRECUENTA] f
  LEFT JOIN [VENDE] v ON f.codigo_tienda = v.codigo_tienda -- Unión en función del codigo_tienda.
  LEFT JOIN [GUSTA] g ON f.cedula = g.cedula AND v.codigo_bebida = g.codigo_bebida -- Unión en función de la cedula y el codigo_bebida.
  
  WHERE f.cedula = b.cedula	-- La cedula en la tabla FRECUENTA coincide con la cedula en la tabla BEBEDOR.
    AND (v.codigo_bebida IS NULL OR g.cedula IS NULL) -- El codigo_bebida en la tabla VENDE o la cedula en la tabla GUSTA es nulo.
) 
-- Si no se encuentra ningún registro en la subconsulta anidada para un bebedor en particular, es decir, 
-- no existen registros en FRECUENTA que cumplan las condiciones mencionadas, entonces ese bebedor se selecciona 
-- y se incluye en los resultados.