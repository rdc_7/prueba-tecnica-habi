-- MUESTRA LA CÉDULA Y EL NOMBRE DE LOS BEBEDORES A LOS QUE LES GUSTA AL MENOS UNA BEBIDA Y QUE FRECUENTAN AL MENOS UNA TIENDA. --

-- Se selecciona los campos cedula y nombre de la tabla BEBEDOR.
SELECT b.cedula, b.nombre FROM [BEBEDOR] b
-- Se hace una unión interna con la tabla GUSTA, lo que significa que solo se seleccionarán los bebedores que estén registrados en la tabla GUSTA.
-- La unión se hace en función de la cédula en ambas tablas.
INNER JOIN [GUSTA] g ON b.cedula = g.cedula
-- Se hace lo mismo con la tabla FRECUENTA, lo que significa que solo se seleccionarán los bebedores que también estén registrados en la tabla FRECUENTA.
INNER JOIN [FRECUENTA] f ON b.cedula = f.cedula;
