-- MUESTRA LA CÉDULA, EL NOMBRE Y LAS BEBIDAS QUE NO LE GUSTAN CORRESPONDIENTES A CADA UNO DE LOS BEBEDORES. --

-- Se selecciona los campos cedula y nombre de la tabla BEBEDOR; y el campo nombre_bebida de la tabla BEBIDA .
SELECT b.cedula, b.nombre, bbd.nombre_bebida FROM [BEBEDOR] b
-- Se hace una unión de producto cartesiano o cruce entre las tablas BEBEDOR y BEBIDA, 
-- lo que significa que se combinarán todas las filas de ambas tablas sin ninguna condición de unión.
CROSS JOIN [BEBIDA] bbd
-- Se usa la cláusula WHERE para comparar cada par de cédula y código de bebida con los pares existentes en la subconsulta extraida de la tabla GUSTA.
WHERE (b.cedula, bbd.codigo_bebida) NOT IN (SELECT cedula, codigo_bebida FROM GUSTA);
-- Si un par (cedula, codigo-bebida) no está presente en la subconsulta, se selecciona y se incluye en los resultados.
