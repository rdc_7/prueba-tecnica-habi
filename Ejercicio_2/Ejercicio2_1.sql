-- MUESTRA LA CÉDULA Y EL NOMBRE DE LOS BEBEDORES QUE NO LES GUSTA LA BEBIDA "COLOMBIANA" --

-- Se selecciona los campos cedula y nombre de la tabla BEBEDOR
SELECT b.cedula, b.nombre FROM [BEBEDOR] b 
-- Se combinan la tabla BEBEDOR con la tabla GUSTA en función del campo cedula.
LEFT JOIN [GUSTA] g ON b.cedula = g.cedula 
-- Se compara el código de la bebida en la tabla GUSTA con el código de la bebida 'Colombiana' en la tabla BEBIDA
AND g.codigo_bebida = 
(
-- Esto asegura que solo se considere la bebida 'Colombiana'.
SELECT codigo_bebida FROM BEBIDA WHERE nombre_bebida = 'Colombiana'
)
-- Se aplica una condición para seleccionar solo aquellos bebedores cuyo código de bebida en la tabla GUSTA es nulo. 
-- Esto significa que no les gusta la bebida 'colombiana'.
WHERE g.codigo_bebida IS NULL
